FROM centos
COPY ./ /project/demo/
WORKDIR /project/demo/
RUN chmod +x start.sh
RUN yum install -y python39
RUN pip3 install -r ./requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
EXPOSE 8000
CMD [ "/bin/sh", "start.sh" ]