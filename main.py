from fastapi import FastAPI
from starlette.routing import Host
import uvicorn

app = FastAPI()

@app.get("/")
async def main():
    return {"hello": "FastAPI1"}

if __name__ == "__main__":
    uvicorn.run(app=app, host="0.0.0.0", port=8000)